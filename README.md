# Asiakaspalvelu widgetin suunnitelma

Tarkoituksena oli suunnitella helppokäyttöinen widget, jonka voisi sisällyttää nettisivulle. Tämän kautta pystyisi keskustelemaan reaaliajassa nettisivun omistajan/asiakaspalvelun kanssa suoraan nettisivulta.